/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : easy-boot-admin

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/01/2022 21:39:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for netdisk_git_auth_info
-- ----------------------------
DROP TABLE IF EXISTS `netdisk_git_auth_info`;
CREATE TABLE `netdisk_git_auth_info`  (
  `id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `owner` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '码云密码',
  `client_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客户ID',
  `client_secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户密码',
  `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'user_info projects pull_requests issues notes keys hook groups gists enterprises' COMMENT '授权范围',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of netdisk_git_auth_info
-- ----------------------------
INSERT INTO `netdisk_git_auth_info` VALUES ('1479711607767568384', '1164719571@qq.com', 'xiaoshihi', '2c05a9f512ca48957b5fa3622d41c546', '05b56977cd1f6edee5b8830c9018027ff8493961401071da8b32db27d95f09f1', 'f769279ec89cfb74d413ef0bee9d67e88f15fc06a6365959ad31f24d7a34540a', 'user_info projects pull_requests issues notes keys hook groups gists enterprises');

-- ----------------------------
-- Table structure for netdisk_git_repo
-- ----------------------------
DROP TABLE IF EXISTS `netdisk_git_repo`;
CREATE TABLE `netdisk_git_repo`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `git_auth_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '归属授权信息',
  `repo` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '仓库名称',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上传目录',
  `open` tinyint(1) NOT NULL DEFAULT 0 COMMENT '仓库是否公开',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '码云仓库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of netdisk_git_repo
-- ----------------------------
INSERT INTO `netdisk_git_repo` VALUES ('1479738660202090496', '1479711607767568384', 'images', 'test/', 0, '呵呵呵杀杀杀');
INSERT INTO `netdisk_git_repo` VALUES ('1479738911684169728', '1479711607767568384', 'music', 'musci', 0, '音乐文件');
INSERT INTO `netdisk_git_repo` VALUES ('1479738911684169729', '1479711607767568384', 'video', 'video', 0, '视频文件');
INSERT INTO `netdisk_git_repo` VALUES ('fc04523f02cc40a8b4ff79cf196ca6df', '1479711607767568384', 'images', 'img', 0, '');

SET FOREIGN_KEY_CHECKS = 1;
