/*
MySQL Data Transfer
Source Host: localhost
Source Database: employeesalary
Target Host: localhost
Target Database: employeesalary
Date: 2017/9/29 23:49:44
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for db_employee
-- ----------------------------
DROP TABLE IF EXISTS `db_employee`;
CREATE TABLE `db_employee` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `employee_no` varchar(64) DEFAULT NULL COMMENT '员工编号',
  `employee_name` varchar(64) DEFAULT NULL COMMENT '员工姓名',
  `card` varchar(100) DEFAULT NULL COMMENT '身份证号',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别',
  `national` varbinary(64) DEFAULT NULL COMMENT '民族',
  `higher_school` varchar(64) DEFAULT NULL COMMENT '毕业学校',
  `job_type` varchar(64) DEFAULT NULL COMMENT '员工岗位',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `join_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '入职时间',
  `pic` varchar(255) DEFAULT NULL COMMENT '头像',
  `role` varchar(64) DEFAULT NULL COMMENT '员工级别',
  `password` varchar(64) DEFAULT NULL COMMENT '登录密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for employee_salary
-- ----------------------------
DROP TABLE IF EXISTS `employee_salary`;
CREATE TABLE `employee_salary` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `work_money` double DEFAULT NULL COMMENT '工资',
  `overtime_money` double DEFAULT NULL COMMENT '加班工资',
  `outwork_money` double DEFAULT NULL COMMENT '出差补贴',
  `total_money` double DEFAULT NULL COMMENT '实际工资',
  `work_year` varchar(10) DEFAULT NULL COMMENT '工资年度',
  `work_month` varchar(40) DEFAULT NULL COMMENT '工作月份',
  `employee_id` varchar(64) NOT NULL COMMENT '员工编号',
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_salary_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `db_employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `db_employee` VALUES ('1098070e26304892a8f3ec8ce81982cd', 'BH1003', '李四', '234234242424234', '男', '瑶族', '清华大学', '技术总监', '35', '2017-09-29 21:13:07', '', '1', '');
INSERT INTO `db_employee` VALUES ('99c1c9e338754e309b2659f7b7a4ad1f', 'BH1004', '张三', '45343434343', '女', '汉', '清华大学', '大客户经理', '30', '2016-09-02 22:46:39', '\\upload\\pic.jpg', '普通员工', '123456');
INSERT INTO `db_employee` VALUES ('9ab2d0ac76064da891e155914e54fff2', 'BH1002', '测试', '45100120203013201', '男', '2', '北京大学', 'java开发', '29', '2017-09-29 21:11:13', '\\upload\\pic.jpg', '1', '');
INSERT INTO `employee_salary` VALUES ('53d6396396984eb29bc0919a1a4e808d', '4000', '800', '500', '5300', '2017', '8月', '99c1c9e338754e309b2659f7b7a4ad1f');
INSERT INTO `employee_salary` VALUES ('d3247969439447c7b2aceb1456d938d7', '3000', '200', '200', '3400', '2017', '1月', '1098070e26304892a8f3ec8ce81982cd');
INSERT INTO `employee_salary` VALUES ('f95cf82026734f1d823beb734c578529', '4000', '0', '0', '4000', '2017', '7月', '99c1c9e338754e309b2659f7b7a4ad1f');
